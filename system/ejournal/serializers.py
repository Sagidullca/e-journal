from rest_framework import serializers

class StudentSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    surname = serializers.CharField(max_length=80, required=False)
    name = serializers.CharField(max_length=80, required=False)
    patronymic = serializers.CharField(max_length=80, required=False)
    group_id = serializers.IntegerField(required=False)
    type = serializers.IntegerField(required= False)


class StudentGroupSerializer(serializers.Serializer):
    id = serializers.IntegerField(required= False)
    number = serializers.IntegerField()
    specialization = serializers.CharField(max_length=200)


class TeacherSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    surname = serializers.CharField(max_length=80)
    name = serializers.CharField(max_length=80)
    patronymic = serializers.CharField(max_length=80)


class DisciplineSerializer(serializers.Serializer):
    TYPE_EXAM = [
        ('N', ''),
        ('Z', 'зачёт'),
        ('O', 'зачёт с оценкой'),
        ('E', 'экзамен')
    ]
    id = serializers.IntegerField()
    name = serializers.CharField(max_length=400)
    short_name = serializers.CharField(max_length=10)
    teacher = TeacherSerializer(required=False)
    course = serializers.IntegerField(required=False)
    hours = serializers.IntegerField(required=False)
    type_exam = serializers.ChoiceField(choices=TYPE_EXAM)


class MarkSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    student_id = serializers.IntegerField(required=False)
    discipline_id = serializers.IntegerField()
    date = serializers.DateField()
    ball = serializers.IntegerField()


class GroupDisciplineSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    discipline = DisciplineSerializer(required=False)
    discipline_id = serializers.IntegerField(required=False)
    type = serializers.IntegerField(required=False)

class GroupEditSerializer(serializers.Serializer):
    group = StudentGroupSerializer(required= True)
    students = StudentSerializer(many=True, required=False)
    disciplines = GroupDisciplineSerializer(many=True, required=False)


