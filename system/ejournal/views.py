
from django.shortcuts import redirect, render
from django.template.response import TemplateResponse
from django.urls import reverse_lazy
from django.views import generic
from .forms import StudentForm
from .models import Student, StudentGroup, Teacher, Discipline, GroupDiscipline, Mark
from rest_framework.generics import  CreateAPIView, GenericAPIView, RetrieveAPIView, DestroyAPIView,RetrieveAPIView, ListAPIView, UpdateAPIView, RetrieveUpdateAPIView, RetrieveUpdateDestroyAPIView
from .serializers import *
from rest_framework.request import Request
from rest_framework.response import Response
from .services.ejournal_service import EjournalService
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from django.http import HttpResponse
service = EjournalService()


def check_perm(request):
    if request.user.is_superuser:
        return redirect('../groups/')
    else:
        return redirect("/student_stat/")

def student_stat(request):
    surname = request.user.studentmodel.surname
    name = request.user.studentmodel.name
    patronymic = request.user.studentmodel.patronymic

    group = request.user.studentmodel.group
    disciplines = group.groupdiscipline_set.all()
    marks = Mark.objects.filter(discipline__in=disciplines, student=request.user.studentmodel)
    dates = marks.dates("date", "day")
    table = []
    title = ["Дисциплины"]
    for date in dates:
        title.append(str(date))
    for discipline in disciplines:
        dis_report = []
        dis_report.append(discipline.discipline.name)
        for date in dates:
            mark = marks.filter(date= date,discipline= discipline)[0]
            dis_report.append(mark.ball)
        table.append(dis_report)

    context = {"titles": title, "reports": table, "surname": surname, "name": name,"patronymic": patronymic, "group": group.number}
    return render(request, "student_info.html", context)



def group_visiting_raiting(request, pk):
    if request.method == "GET":
        group = StudentGroup.objects.get(id=pk)
        students = Student.objects.filter(group=group)
        group_disciplines = GroupDiscipline.objects.filter(group=group)
        raiting_list = []
        for student in students:
            cnt_pass = 0
            for discipline in group_disciplines:
                cur_marks = Mark.objects.filter(student=student, discipline=discipline)
                for mark in cur_marks:
                    if mark.ball == -1:
                        cnt_pass += 1
            cur_student = [student, cnt_pass]
            raiting_list.append(cur_student)
        raiting_list.sort(key=lambda x: x[1])
        #raiting_list = map(lambda x: x[0], raiting_list)
        titles = ["ФИО", "Количество пропусков"]
        context = {"students": raiting_list, "groupmodel": group, "titles": titles}
        return  render(request, "ejournal/group_raiting.html",context)


class Student_Stat():
    def __init__(self):
        self.fio = None
        self.study_ball = 0
        self.addres_ball = 0
        self.cnt_pass = 0



def rating_log(request, pk):
    if request.method == "GET":
        curDis = request.GET.get("choose_discipline")
        if curDis == None:
            group = StudentGroup.objects.get(id=pk)
            context = {"group": group}
            return render(request, "ejournal/raiting_log.html", context)
        else:
            discipline = Discipline.objects.get(name=curDis)
            group = StudentGroup.objects.get(id=pk)
            groupDiscipline = GroupDiscipline.objects.get(discipline=discipline, group=group)
            students = Student.objects.filter(group=group)
            curMarks = Mark.objects.filter(discipline=groupDiscipline)
            studentsMark =curMarks.filter(student__in=students)
            dates =set(curMarks.values_list('date',flat=True))
            strDate =sorted( list(map(lambda x: x.strftime("%Y-%m-%d"), dates)))
            marks = []
            for student in students:
                marks.append({"student": student, "marks":[]})
                for date in strDate:
                    if Mark.objects.filter(student=student, date=date,discipline=groupDiscipline).exists():
                        curMark = Mark.objects.get(student=student, date=str(date), discipline=groupDiscipline)
                        if curMark.ball == -1:
                            marks[-1]["marks"].append('н')
                        else:
                            marks[-1]["marks"].append(curMark.ball)
                    else:
                        marks[-1]["marks"].append(0)
            context = {"group": group, "students": students, "marks": curMarks, "dates": strDate, "stmarks": marks, "cur_discipline": discipline}

            return render(request, "ejournal/raiting_log.html", context)
    else:
        curDis = request.GET.get("choose_discipline")
        dates = request.POST.getlist("data")
        ocenkas = request.POST.getlist('ocenka')
        group = StudentGroup.objects.get(id=pk)
        discipline = Discipline.objects.get(name=curDis)
        groupDiscipline = GroupDiscipline.objects.get(discipline=discipline, group=group)
        students =list(Student.objects.filter(group=group))
        curMarks = Mark.objects.filter(discipline=groupDiscipline)
        studentsMark = curMarks.filter(student__in=students)
        studentsMark.delete()
        i =1
        j=0
        curStudent= students[j]
        for ocenka in ocenkas:
            if ocenka == '' or dates[i-1] == '':
                continue
            mark = Mark()
            mark.student = curStudent
            if ocenka == 'н':
                mark.ball = -1
            else:
                mark.ball = ocenka
            mark.date =dates[i-1]
            mark.discipline = groupDiscipline
            mark.save()
            if i == len(dates):
                j +=1
                if(j==len(students)):
                    break
                curStudent= students[j]
                i=1
            else:
                i += 1

        return redirect(group)



class groupUpdateView(generic.UpdateView):
    model = StudentGroup
    fields = ["number", "departament", "specialization"]


    def get_context_data(self, *args, **kwargs):
        context = super(groupUpdateView, self).get_context_data(**kwargs)
        context['disciplines'] =disciplines
        return context


    def post(self, request, *args, **kwargs):
        # получение значений студентов
        id = request.POST.getlist('id')
        name = request.POST.getlist('name')
        surname =request.POST.getlist('surname')
        patronymic = request.POST.getlist("patronymic")
        # получение текущего взвода
        group = StudentGroup.objects.get(id=kwargs['pk'])
        # получение id существующих студентов, так как клиент для новых студентов возвращает пустую строку как id
        id_upd = list(filter(lambda x: x!='',id))
        students =Student.objects.filter(group=group)
        students_upd =students.filter(id__in= id_upd)
        # id всех студентов
        students_id = map(lambda x: x.id,students )
        # id студентов, информацию о которых надо обновить
        students_upd_id = map(lambda x: x.id, students_upd)
        del_students = set(students_id).difference(set(students_upd_id))
        for student_id in del_students:
            student = Student.objects.get(id=student_id)
            student.delete()
        for x in range(len(id)):
            # изменение уже сущетсвующих элементов
            if(id[x]!=''):
                student = Student.objects.get(id=id[x])
                student.name = name[x]
                student.surname = surname[x]
                student.patronymic = patronymic[x]
                student.save()
            else:
                student = Student()
                student.name = name[x]
                student.surname = surname[x]
                student.patronymic = patronymic[x]
                student.group=group
                student.save()

        disciplines = request.POST.getlist("discipline") # список названий дисциплин
        discipline_pk = request.POST.getlist("discipline_pk")[1:] # спискок их id
        Addres_pk = GroupDiscipline.objects.get(discipline= Discipline.objects.get(name="дополнительные обязанности"), group=group).pk # id дисциплны доп обяз
        # получение id существующих дисциплин, так как клиент для новых дисциплин возвращает пустую строку как id
        id_discp_upd = list(filter(lambda x: x != '' and x !=str(Addres_pk), discipline_pk))
        groupDiscipline = GroupDiscipline.objects.filter(group=group)
        groupDiscipline = GroupDiscipline.objects.exclude(discipline= Discipline.objects.get(name="дополнительные обязанности"))
        group_dicp_upd = GroupDiscipline.objects.filter(id__in=id_discp_upd)
        # id всех дисциплин
        discipline_id = map(lambda x: x.id, groupDiscipline)
        # id дисциплин, информацию о которых надо обновить
        discipline_upd_id = map(lambda x: x.id, group_dicp_upd)
        del_discipline = set(discipline_id).difference(set(discipline_upd_id))
        # удаление
        for disc_id in del_discipline:
            discipline = GroupDiscipline.objects.get(id=disc_id)
            discipline.delete()

        for x in range(len(discipline_pk)):
            # изменение уже сущетсвующих элементов
            if (discipline_pk[x] != '' and discipline_pk[x] != str(Addres_pk)):
                discipline = GroupDiscipline.objects.get(id=discipline_pk[x])
                discipline.group = group
                dirDiscp = Discipline.objects.get(name=disciplines[x])
                discipline.discipline =dirDiscp
                discipline.save()
            elif (discipline_pk[x] != str(Addres_pk)):
                discipline = GroupDiscipline()
                discipline.group = group
                dirDiscp = Discipline.objects.get(name=disciplines[x])
                discipline.discipline = dirDiscp
                discipline.save()

        return super().post(request, *args, **kwargs)



class GroupListRest(ListAPIView):
    serializer_class = StudentGroupSerializer
    renderer_classes =  [JSONRenderer]

    def get(self, requset: Request) -> Response:
        response = service.get_all_group()
        return Response(data= response.data)

class GroupDeleteRest(DestroyAPIView):
    serializer_class =  StudentGroupSerializer
    renderer_classes = [JSONRenderer]

    def delete(self, request, group_id: int)-> Response:
        service.delete_group(group_id)
        return  Response(status=status.HTTP_200_OK)

class GroupDetailRest(RetrieveUpdateDestroyAPIView):
    serializer_class =  GroupEditSerializer
    renderer_classes = [JSONRenderer]

    def get(self, request: Request, id: int):
        response = service.get_group(id)
        if response is not None:
            return Response(data= response.data)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def delete(self, request: Request, *args, **kwargs):
        group_id = int(kwargs["id"])
        service.delete_group(group_id)
        return Response(status=status.HTTP_200_OK)


class GroupCreateRest(CreateAPIView):
    serializer_class =  StudentGroupSerializer
    renderer_classes = [JSONRenderer]

    def post(self, request, *args, **kwargs) -> Response:
        serializer = StudentGroupSerializer(data=request.data)
        if serializer.is_valid():
            service.add_group(serializer)
            return Response(status = status.HTTP_201_CREATED)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class TeacherCreateRest(CreateAPIView):
    serializer_class =  TeacherSerializer
    renderer_classes = [JSONRenderer]

    def post(self, request, *args, **kwargs):
        serializer = TeacherSerializer(data= request.data)
        if serializer.is_valid():
            service.add_teacher(serializer)
            return  Response(status= status.HTTP_201_CREATED)
        return  Response(status.HTTP_500_INTERNAL_SERVER_ERROR)

class TeacherListRest(ListAPIView):
    serializer_class = TeacherSerializer
    renderer_classes = [JSONRenderer]

    def get(self, request, *args, **kwargs):
        response = service.get_all_teachers()
        return  Response(data= response.data)

class TeacherDeleteRest(RetrieveAPIView):
    serializer_class = TeacherSerializer
    renderer_classes = [JSONRenderer]

    def delete(self, request, *args, **kwargs) -> Response:
        service.delete_teacher(int(kwargs["id"]))
        return Response(status=status.HTTP_200_OK)

class TeacherDetailRest(RetrieveAPIView):
    serializer_class =  TeacherSerializer
    renderer_classes = [JSONRenderer]

    def get(self, request: Request, id: int):
        response = service.get_teacher(id)
        if response is not None:
            return Response(data= response.data)
        return Response(status=status.HTTP_204_NO_CONTENT)

class TeacherUpdate(UpdateAPIView):
    serializer_class =  TeacherSerializer
    renderer_classes =  [JSONRenderer]

    def patch(self, request, id, *args, **kwargs):
        serializers = TeacherSerializer(data= request.data)
        if serializers.is_valid():
            service.update_teacher(serializers)
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class DisciplineListRest(ListAPIView):
    serializer_class= DisciplineSerializer
    renderer_classes =  [JSONRenderer]

    def get(self, request, *args, **kwargs):
        respose = service.get_all_discipline()
        return Response(data = respose.data)


class DisciplineCreateRest(CreateAPIView):
    serializer_class= DisciplineSerializer
    renderer_classes =  [JSONRenderer]

    def post(self, request, *args, **kwargs):
        serializer = DisciplineSerializer(data= request.data)
        if serializer.is_valid():
            service.add_discipline(serializer)
            return Response(status=status.HTTP_201_CREATED)
        return Response(status.HTTP_500_INTERNAL_SERVER_ERROR)

class DisciplineDelete(DestroyAPIView):
    serializer_class= DisciplineSerializer
    renderer_classes =  [JSONRenderer]

    def delete(self, request, id, *args, **kwargs):
        service.delete_discipline_by_id(id)
        return Response(status=status.HTTP_200_OK)


class DisciplineDetail(RetrieveUpdateAPIView):



    serializer_class = DisciplineSerializer
    renderer_classes = [JSONRenderer]

    def get(self, request, id, *args, **kwargs):
        response = service.get_discipline(id)
        if response is not None:
            return Response(data=response.data)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, *args, **kwargs):
        serializers = DisciplineSerializer(data= request.data)
        if serializers.is_valid():
            service.update_discipline(serializers)
            return Response(status=status.HTTP_200_OK)
        return Response(status.HTTP_500_INTERNAL_SERVER_ERROR)


class GroupEdit(UpdateAPIView):
    serializer_class = GroupEditSerializer
    renderer_classes = [JSONRenderer]

    def put(self, request, id, *args, **kwargs):
        serializers = GroupEditSerializer(data= request.data)
        if serializers.is_valid():
            service.update_group(id, serializers)
            return Response(status= status.HTTP_200_OK)
        return Response(status= status.HTTP_500_INTERNAL_SERVER_ERROR)



class MarkUpdate(RetrieveUpdateAPIView):
    serializer_class = MarkSerializer
    render_classes = [JSONRenderer]

    def get(self, request, *args, **kwargs):
        group_id = kwargs["id"]
        dis_id = request.query_params["dis"]
        response = service.get_marks(group_id, dis_id)
        return Response(data=response.data)


    def put(self, request, *args, **kwargs):
        ser = MarkSerializer(data= request.data, many=True)
        ser.is_valid()
        group_id = kwargs["id"]
        service.updateMarks(ser, group_id)
        return Response(status=status.HTTP_200_OK)




def docs(request):
    return HttpResponse("It is docs.")








