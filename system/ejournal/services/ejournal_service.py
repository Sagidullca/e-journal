
from ..serializers import *
from .repository_service import *



class EjournalService:


    def add_student(self,group_id,  student: StudentSerializer) -> None:
        group = get_group_by_id(group_id)
        create_student(student.get('surname'),
                       student.get('name'),
                       student.get('patronymic'),
                       group)

    def add_group(self, group: StudentGroupSerializer):
        group_data = group.data
        create_group(group_data.get('number'),
                     group_data.get('specialization'))


    def get_all_group(self) -> Optional[StudentGroupSerializer]:
        groups = get_all_groups()
        if groups is not None:
            return  StudentGroupSerializer(groups, many=True)
        return groups

    def delete_group(self, groups_id: int) -> None:
        delete_group_by_id(id=groups_id)


    def get_group(self, id: int) -> Optional[GroupEditSerializer]:
        group = get_group_by_id(id)
        students = get_students_by_group(group)
        disciplines = get_group_discipline(group)
        if group is not None:
            return GroupEditSerializer({"group":group, "students": students, "disciplines": disciplines})
        return group

    def add_teacher(self, teacher: TeacherSerializer) -> None:
        teacher_data = teacher.data
        create_teacher(
            teacher_data.get("surname"),
            teacher_data.get("name"),
            teacher_data.get("patronymic")
        )

    def get_all_teachers(self) -> Optional[TeacherSerializer]:
        teachers = get_all_teachers()
        if teachers is not None:
            return TeacherSerializer(teachers, many=True)
        return teachers

    def delete_teacher(self, teacher_id) -> None:
        delete_teacher_by_id(teacher_id)


    def get_teacher(self, id) -> Optional[TeacherSerializer]:
        teacher = get_teacher_by_id(id)
        if teacher is not None:
            return TeacherSerializer(teacher)
        return teacher

    def get_all_discipline(self) -> Optional[DisciplineSerializer]:
        disciplines = get_all_discipline()
        if disciplines is not None:
            return DisciplineSerializer(disciplines, many=True)
        return disciplines

    def add_discipline(self, discipline: DisciplineSerializer):
        discipline_data = discipline.data
        create_discipline(
            discipline_data.get("name"),
            discipline_data.get("short_name"),
            int(discipline_data.get("teacher_id")),
            int(discipline_data.get("course")),
            int(discipline_data.get("hours")),
            discipline_data.get("type_exam")
        )

    def delete_discipline_by_id(self, id):
        delete_discipline_by_id(id)

    def get_discipline(self, id):
        discipline =get_discipline_by_id(id)
        if discipline is not None:
            return DisciplineSerializer(discipline)
        return discipline

    def update_group(self, id: int, ser: GroupEditSerializer):
        data = ser.data
        groupSer = data.get("group")
        update_group(id, int(groupSer.get("number")), groupSer.get("specialization"))

        students = data.get("students")
        for student in students:
            if (student.get("type") == None or int(student.get("type")) == 0):
                continue
            if (int(student.get("type")) == -1):
                delete_student_by_id(int(student.get("id")))
            if (int(student.get("type")) == 1):
                self.add_student(id, student)

        disciplines = data.get("disciplines")
        for discipline in disciplines:
            if (discipline.get("type") == None or  int(discipline.get("type")) == 0):
                continue
            if (int(discipline.get("type")) == -1):
                delete_group_discipline(discipline)
            if (int(discipline.get("type")) == 1):
                add_group_discipline(id, discipline)

    def update_teacher(self, teacher: TeacherSerializer):
        update_teacher(int(teacher.data.get("id")), teacher.data.get("surname"),
                       teacher.data.get("name"),
                       teacher.data.get("patronymic"))



    def update_discipline(self, discipline: DisciplineSerializer):
        data = discipline.data
        ts = get_teacher_by_id(data.get("teacher")["id"])
        update_discipline(int(data.get("id")),
                          data.get("name"),
                          data.get("short_name"),
                          int(data.get("course")),
                          int(data.get("hours")),
                          ts,
                          data.get("type_exam"))

    def updateMarks(self, data, group_id):
        ser =data
        marks = ser.data
        if len(marks):
            delete_all_groups_marks(group_id , marks[0].get("discipline_id"))
        for mark in marks:
            add_mark(mark.get("student_id"), mark.get("discipline_id"), mark.get("date"), mark.get("ball"))



    def get_marks(self, group_id, dis_id):
        group = StudentGroup.objects.get(id=group_id)
        students = Student.objects.filter(group=group)
        discipline = GroupDiscipline.objects.get(id=dis_id)

        return MarkSerializer(Mark.objects.filter(student__in=students,discipline=discipline), many=True)
