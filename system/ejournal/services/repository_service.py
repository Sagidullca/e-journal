from typing import Optional

from django.core.exceptions import ObjectDoesNotExist
from django.db.models import QuerySet

from ..models import Student, StudentGroup, Teacher, Discipline, GroupDiscipline, Mark


def get_students_by_group(group: StudentGroup) -> QuerySet:
    return Student.objects.filter(group=group).all()


def create_group(number: int, specialization: str) -> None:
    group = StudentGroup.objects.create(number=number, specialization=specialization)
    group.save()


def create_student(surname: str, name: str, patronymic: str, group: StudentGroup) -> int:
    student = Student.objects.create(surname=surname, name=name, patronymic=patronymic, group=group)
    student.save()
    return student.id


def update_student_group_by_id(id: int, group: StudentGroup) -> None:
    student = get_student_by_id(id=id)
    student.group = group
    student.save()


def get_student_by_id(id: int) -> Optional[Student]:
    try:
        return Student.objects.get(id=id)
    except ObjectDoesNotExist:
        return None


def delete_student_by_id(id: int) -> None:
    try:
        get_student_by_id(id).delete()
    except ObjectDoesNotExist:
        return


def get_group_by_number(number: int) -> Optional[StudentGroup]:
    try:
        group = StudentGroup.objects.get(number=number);
    except ObjectDoesNotExist:
        return None
    return group


def get_group_by_id(id: int) -> Optional[StudentGroup]:
    try:
        group = StudentGroup.objects.get(id=id);
    except ObjectDoesNotExist:
        return None
    return group


def delete_group_by_number(number: int) -> None:
    get_group_by_number(number).delete()


def create_teacher(surname: str, name: str, patronymic: str) -> None:
    teacher = Teacher.objects.create(surname=surname, name=name, patronymic=patronymic)
    teacher.save()


def get_teacher_id_by_fio(surname: str, name: str, patronymic: str) -> int:
    try:
        teacher = Teacher.objects.get(surname=surname, name=name, patronymic=patronymic)
    except ObjectDoesNotExist:
        return -1;
    return teacher.id


def delete_teacher_by_id(id: id) -> None:
    try:
        Teacher.objects.get(id=id).delete()
    except ObjectDoesNotExist:
        return


def update_group_specialization_by_number(number: int, new_specialization: str) -> None:
    group = get_group_by_number(number)
    if (group == None):
        return
    group.specialization = new_specialization
    group.save()


def update_group(id: int, number: int, spc: str) -> None:
    group = get_group_by_id(id)
    group.number = number
    group.specialization = spc
    group.save()


# def create_student(name: str, short_name: str, teacher_id: id, group: StudentGroup) -> int:
#     student = Student.objects.create(surname=surname, name=name, patronymic=patronymic, group=group)
#     student.save()
#     return student.id

def get_all_groups():
    groups = StudentGroup.objects.all()
    return groups


def delete_group_by_id(id: int) -> None:
    group = StudentGroup.objects.get(id=id)
    students = Student.objects.filter(group=group)
    disciplines = GroupDiscipline.objects.filter(group=group)
    disciplines.delete()
    students.delete()
    group.delete()


def get_all_teachers() -> QuerySet:
    teachers = Teacher.objects.all()
    return teachers


def get_teacher_by_id(id) -> Optional[Teacher]:
    try:
        return Teacher.objects.get(id=id)
    except ObjectDoesNotExist:
        return None


def update_teacher(id, surname, name, patronymic) -> None:
    old_teacher = Teacher.objects.get(id=id)
    old_teacher.surname = surname
    old_teacher.name = name
    old_teacher.patronymic = patronymic
    old_teacher.save()


def update_discipline(id, name, short_name, course, hours, teacher, type_exam):
    discipline = Discipline.objects.get(id=id)
    discipline.name = name
    discipline.short_name = short_name
    discipline.course = course
    discipline.hours
    discipline.teacher = teacher
    discipline.type_exam = type_exam
    discipline.save()


def get_all_discipline() -> QuerySet:
    disciplines = Discipline.objects.all()
    return disciplines


def create_discipline(name: str, short_name: str, teacher_id: int, course: int, hourse: int, type_exam: str) -> None:
    teacher = Teacher.objects.get(id=teacher_id)
    discpline = Discipline.objects.create(name=name, short_name=short_name, teacher=teacher, course=course,
                                          hours=hourse, type_exam=type_exam)
    discpline.save()


def delete_discipline_by_id(id):
    try:
        Discipline.objects.get(id=id).delete()
    except ObjectDoesNotExist:
        None


def get_discipline_by_id(id):
    try:
        return Discipline.objects.get(id=id)
    except ObjectDoesNotExist:
        return None


def delete_all_groups_marks(group_id: int, discipline_id: int):
    group = StudentGroup.objects.get(id=group_id)
    students = Student.objects.filter(group=group)
    discipline = GroupDiscipline.objects.get(id=discipline_id)
    for student in students:
        Mark.objects.filter(student=student, discipline=discipline).delete()


def add_mark(student_id: int, discipline_id: int, date, ball: int):
    discipline = GroupDiscipline.objects.get(id=discipline_id)
    student = Student.objects.get(id=student_id)
    mark = Mark.objects.create(ball=ball, discipline=discipline, student=student,
                               date=date)
    mark.save()


def get_group_discipline(group):
    return GroupDiscipline.objects.filter(group=group).all()


def delete_group_discipline(discipline):
    id = int(discipline.get("id"))
    GroupDiscipline.objects.get(id=id).delete()

def add_group_discipline(group_id, discipline):
    group = StudentGroup.objects.get(id=group_id)
    discipline = Discipline.objects.get(id=int(discipline.get("discipline_id")))
    GroupDiscipline.objects.create(group= group, discipline=discipline)
