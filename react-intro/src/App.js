
import './App.css';
import GroupList from "./components/groupList"
import { BrowserRouter as Router, Routes, Route, Link}
    from 'react-router-dom';
import GroupDetail from './components/groupDetail';
import GroupCreate from './components/groupCreate';
import TeacherList from './components/TeacherList';
import DisciplineList from './components/DisciplineList';
import TeacherDetail from './components/TeacherDetail'
import TeacherCreate from './components/TeacherCreate'
import TeacherEdit from './components/TeacherEdit'
import DisciplineDetail from './components/DisciplineDetail'
import DisciplineEdit from './components/DisciplineEdit';
import DisciplineCreate from './components/DisciplineCreate';
import GroupEdit from './components/groupEdit';
import MarkList from './components/MarkList';


function App() {

  
  return (

  <div>
     <Router>

     <nav className="uk-navbar-container">
      <div className="uk-navbar-center">
      <ul className="uk-navbar-nav">
          <li><Link to={'/e-journal/'}>Группы</Link></li>
          <li><Link to={'/e-journal/teachers'}>Преподаватели</Link></li>
          <li><Link to={'/e-journal/disciplines'}>Дисциплины</Link></li>
        </ul>
      </div>
    </nav>
    <Routes>
      <Route path="/e-journal/group/mark/:id" element = {<MarkList/>}/>
      <Route path="/e-journal/group/edit/:id" element = {<GroupEdit/>}/>
      <Route path="/e-journal/group/create" element = {<GroupCreate />} />
      <Route path="/e-journal/group/:id" element = { <GroupDetail/>}/>
      <Route path="/e-journal/discipline/edit/:id" element ={<DisciplineEdit/>}/>
      <Route path='/e-journal/discipline/:id' element= {<DisciplineDetail/> }/>
      <Route path='/e-journal/discipline/create' element = {<DisciplineCreate/>}/>
      <Route path='/e-journal/disciplines/' element = {<DisciplineList/>}/>
      <Route path="/e-journal/teacher/create" element = {<TeacherCreate/>}/>
      <Route path="/e-journal/teacher/edit/:id" element = {<TeacherEdit/>}/>
      <Route path="/e-journal/teacher/:id" element = {<TeacherDetail/> }/>
      <Route path="/e-journal/teachers/" element = {<TeacherList/>}/>
      <Route path="/e-journal/" element={<GroupList/>}>
      
      </Route>
    
      
    </Routes>
    </Router>
  </div>
  );
}

export default App;
