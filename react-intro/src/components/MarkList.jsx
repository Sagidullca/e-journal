import React  from "react";
import axios from "axios"
import withRouter from '../withRouter'
import { ThemeConsumer } from "styled-components";
import { type } from "@testing-library/user-event/dist/type";



class MarkList extends React.Component {

    constructor(props){
        super(props);
        this.state ={id : props.params.id, total_mark: [], cur_disc: -1, datas: [], marks: []}

        
    }

    updateData = (event) => {
        console.log('POST Request to: ' + "http://192.168.56.104:8080/api/group/edit/")
        // получаем Id населённого пункта из словаря и меняем состояние через встроенный метод класса React.Component setState
        event.preventDefault();   // необходимо, чтобы отключить стандартное поведение формы в браузере (AJAX)
        // формируем данные для отправки на сервер
        let mks = [];
        this.state.datas.map((dat, index) => (this.state.students.map((studen, i) =>
            mks.push({date: dat, discipline_id: this.state.cur_disc, student_id: Number(studen.id), ball: this.state.marks[index][i]})
        )));
     
        // HTTP-клиент axios автоматически преобразует объект data в json-строку
        axios.put("http://192.168.56.104:8080/api/group/mark/" +this.state.id, mks, {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json;charset=UTF-8",
          },
        })
        .then(response => {
          console.log('Response: ' + response.status);
          window.location.href = '/e-journal/'
        }, error => {
            console.log(error);
            alert(error);
        });
      }

    
    
    
    getData(){
        console.log('Get Request to: ' + "http://192.168.56.104:8080/api/group/mark"+this.state.id)
        axios.get('http://192.168.56.104:8080/api/group/'+this.state.id)
        .then(response => {
            this.setState(state => ({number: response.data.group.number}));
            this.setState(state => ({specialization: response.data.group.specialization}));
            this.setState(state => ({students: response.data.students}));
            this.setState(state => ({disciplines: response.data.disciplines}));
            this.setState(state => ({gettingData: true}))          
        }, error => {this.setState(state => ({groupData: [],})); console.log(error);
    });
    }

    igetMarks(cur_disc){
        if (this.state.students != null){
            axios.get('http://192.168.56.104:8080/api/group/mark/'+this.state.id+"?dis="+cur_disc)
            .then(response => {
                var d= response.data;
                var l = d.length;
                let ds = [...new Set(d.map(a => a.date))]; 
                let msk = [];
                for(let i=0; i<ds.length; i++){
                    let mk =[];
                    for(let j =0; j<this.state.students.length; j++){
                        for(let k=0; k< l; k++){
                            if(d[k].date == ds[i] && d[k].student_id == this.state.students[j].id)
                               mk = [...mk, d[k].ball]
                        }
                    }
                    msk = [...msk, mk]
                }



                this.setState(state => ({datas: ds}))
                this.setState(state => ({marks: msk}))
                
            }, error => {this.setState(state => ({groupData: [],})); console.log(error);
            });  
    }
    }

      
    componentDidMount() {
        this.getData(); 
    }

    renderMarksTable(){

        return(
            <table  className="uk-table uk-table-striped">
               <tr><th>Дата</th>{this.state.students.length >=1 && this.state.students.map(data => (<th>{data.name +" "+ data.surname}</th>))}</tr>
               <tbody>
                {this.state.datas.map((data, index) => (<tr><td><input className="uk-input" value={data} onChange={(e) =>this.dataChangeHandler(e, index)} type="date"></input></td>{this.state.marks[index].map((c, i) => (<td><input value={c} className="uk-input" type="number" min="1" max="5" onChange={(e) => this.markChange(index, i, e)}/></td>))}</tr>))}
               </tbody>
            </table>


        )
    }
    
    markChange(row, cols, event){
        const copy = [...this.state.marks]
        copy[row][cols] = event.target.value
        this.setState(state => ({marks: copy}))
    }

    dataChangeHandler(event, index){
        const copy = [...this.state.datas];
        copy[index] = event.target.value
        this.setState(state => ({datas: copy}))
    }

    changeNoteDiscipline(index, event) {
        const copy = [...this.state.disciplines];
        copy[index].discipline_id = event.target.value;
        this.setState(state => ({disciplines: copy}));
    }

    changeNoteStudent(index, event) {
        const copy = [...this.state.students];
        copy[index][event.target.name] = event.target.value;
        this.setState(state => ({students: copy}));
    }

    addMarks(){

        let data = new Array(this.state.students.length).fill(0)
            this.setState(st => ({marks: [...st.marks, data]}))        
            this.setState(st => ({datas: [...st.datas, ""]}))
            
    }          
    

    getMarks(event){
        this.setState(state => ({cur_disc: event.target.value}))
        this.igetMarks(event.target.value);
        
    }

    render(){
        if (this.state.gettingData != null)
        return (

            <form onSubmit={this.updateData} className="uk-form-stacked">
                <div className="uk-section uk-section-muted">
                    <div  className="uk-margin uk-card uk-card-default uk-card-body">
                        <h1>Номер группы: {this.state.number}</h1>
                        <select className="uk-select" value={this.state.cur_disc} onChange={(e) =>this.getMarks(e)}>
                           <option>-выберите предмет-</option>
                            { this.state.disciplines.map((data) => (<option value={data.id}>{data.discipline.name}</option>))}
                        </select>
                       {this.state.cur_disc != -1 && this.renderMarksTable()}
                       <button type="button" className="uk-button" onClick={this.addMarks.bind(this)}>Добавить занятие</button>
                        <input type="submit" value="Сохранить" className="uk-button uk-button-primary"/>
                   
                    </div>
                </div>
                
            </form>
        )

    }


}


export default withRouter(MarkList)