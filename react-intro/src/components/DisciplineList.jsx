import React from "react";
import axios from "axios"
import {
    Link,
  } from 'react-router-dom';


class DisciplineList extends React.Component {

    getData(){
        console.log('Get Request to: ' + "http://192.168.56.104:8080/api/disciplines")
        axios.get("http://192.168.56.104:8080/api/disciplines")
        .then(response => {
            this.setState(state => ({disciplineData: response.data}));
        }, error => {this.setState(state => ({disciplineData: [],})); console.log(error);
    });

    }

    constructor(props){
        super(props);
        this.state = {disciplineData: []};
    }

    componentDidMount() {
        this.getData(); 
    }


    renderData() {
        if (this.state.disciplineData.length > 0 ){
            return this.state.disciplineData.map((dataRow) =>{
                return (
                    <Link key={dataRow.id} to={`/e-journal/discipline/${dataRow.id}`}>
                        <li>{dataRow.name }</li>
                    </Link> 
                )
            })
        }
        else {
            return(
                <div className="uk-alert-danger">
                    <a className="uk-alert-close"></a>
                    <p>NO disciplines</p>
                </div>  
            )
        }
    }

    render() {
        return(
        <div className="uk-section uk-section-muted">
            <div  className="uk-margin uk-card uk-card-default uk-card-body">
                <h1>Список учебеных дисциплин</h1>
                <Link key="create" to="/discipline/create">
                    <a>Добавить новую дисциплину</a>
                </Link>
                <br/>
               
                        <ul>
                            {this.renderData()}
                        </ul>
                
            </div>
        </div>
        );
    }
}

export default DisciplineList