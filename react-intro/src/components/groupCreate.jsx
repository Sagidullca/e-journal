import React  from "react";
import axios from "axios"
import withRouter from '../withRouter'



class GroupCreated extends React.Component {

    constructor(props){
        super(props);
    
    
        
    }

    updateData = (event) => {
        console.log('POST Request to: ' + "http://192.168.56.104:8080/api/group/")
        // получаем Id населённого пункта из словаря и меняем состояние через встроенный метод класса React.Component setState
        event.preventDefault();   // необходимо, чтобы отключить стандартное поведение формы в браузере (AJAX)
        // формируем данные для отправки на сервер
        let data = {
          number: parseInt(this.state.number), 
          specialization: (this.state.spesial), 
          
        };
        // HTTP-клиент axios автоматически преобразует объект data в json-строку
        axios.post("http://192.168.56.104:8080/api/group/create", data, {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json;charset=UTF-8",
          },
        })
        .then(response => {
          console.log('Response: ' + response.status);
          window.location.href = '/e-journal/'
        }, error => {
            console.log(error);
            alert(error);
        });
      }

    render(){
        return (
            <form onSubmit={this.updateData} className="uk-form-stacked">
                <div className="uk-section uk-section-muted">
                    <div  className="uk-margin uk-card uk-card-default uk-card-body">
                        <h1>Номер группы:</h1><input className= "uk-input" id="groupNumber" type="number" minLength="4" maxLength="8" onChange={(e) => {this.setState({number: e.target.value})}}></input>
                        <p><strong>Специальность:</strong> <input className= "uk-input" type="text" maxLength="300" onChange={(e) => {this.setState({spesial: e.target.value})}}/></p>
                        <input type="submit" value="Сохранить" className="uk-button uk-button-primary"/>
                    </div>
                </div>
            </form>
        )

    }


}


export default withRouter(GroupCreated)