import React from "react";
import axios from "axios"

import withRouter from '../withRouter'


class DisciplineCreate extends React.Component {

    constructor(props){
        super(props);
        this.state ={id : props.params.id, teachers: []}
    
    }

    getData(){
        axios.get('http://192.168.56.104:8080/api/teachers').then(
            response => {
                this.setState(state => ({teachers:  response.data}));
            }, 
            error => {this.setState(state => ({teachers: []}));
             console.log(error);});

    }

    componentDidMount() {
        this.getData(); 
    }



    updateData = (event) => {
        console.log('POST Request to: ' + "http://192.168.56.104:8080/api/discipline/edit")
        event.preventDefault(); 
        // формируем данные для отправки на сервер
        let data = {
          id: (this.state.id),
          name: (this.state.name), 
          short_name: (this.state.short_name), 
          course: (this.state.course),
          hours: (this.state.hours),
          type_exam: (this.state.type_exam),
          teacher: (this.state.teachers.filter(dataRow =>  dataRow.id == this.state.set_teacher)[0]),
        };
        // HTTP-клиент axios автоматически преобразует объект data в json-строку
        axios.post("http://192.168.56.104:8080/api/discipline/create", data, {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json;charset=UTF-8",
          },
        })
        .then(response => {
          console.log('Response: ' + response.status);
          window.location.href = '/e-journal/disciplines/'
        }, error => {
            console.log(error);
            alert(error);
        });
      }




    render(){
        if  (this.state.teachers.length > 0)
        return (
           
                <form onSubmit={this.updateData} className="uk-form-stacked">
                    <div className="uk-section uk-section-muted">
                        <div  className="uk-margin uk-card uk-card-default uk-card-body">
                            <label className="uk-form-label">Название</label>
                            <input className= "uk-input"  type="text" minLength="4" maxLength="20" onChange={(e) => {this.setState({name: e.target.value})}}></input>
                            <label className="uk-form-label">Сокращение</label>
                            <input className= "uk-input"  type="text" minLength="4" maxLength="20"  onChange={(e) => {this.setState({short_name: e.target.value})}}></input>
                            <label className="uk-form-label">Преподаватель</label>
                            { (this.state.teachers.length > 0) &&
                                <select className="uk-select"  value={this.state.set_teacher} onChange={(e) => {this.setState({set_teacher: e.target.value})}}>
                                    {this.state.teachers.map((dataRow) => <option value={dataRow.id} > {dataRow.surname +" "+ dataRow.name}</option>)}
                                </select>
                            }
                            
                           <label className="uk-form-label">Курс</label>
                            <input className= "uk-input"  type="number" minLength="4" maxLength="20"   onChange={(e) => {this.setState({course: e.target.value})}}></input>
                            <label className="uk-form-label">Количество часов</label>
                            <input className= "uk-input"  type="number" minLength="4" maxLength="20"  onChange={(e) => {this.setState({hours: e.target.value})}}></input>
                            <label className="uk-form-label">Тип экзамена</label>
                            <select className="uk-select"  onChange={(e) => {this.setState({type_exam: e.target.value})}}>
                                     <option value="N" > Не известно</option>
                                     <option value="Z">Зачет</option>
                                     <option value="O">Зачет с оценкой</option>
                                     <option value="E">Экзамен</option>
                                </select>
                            <input type="submit" value="Сохранить" className="uk-button uk-button-primary uk-margin-small"/>
                        </div>
                    </div>
                </form>
                        
)

    }


}


export default withRouter(DisciplineCreate)