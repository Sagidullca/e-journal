import React from "react";
import axios from "axios"

import withRouter from '../withRouter'



class TeacherDetail extends React.Component {

    constructor(props){
        super(props);
        this.state ={id : props.params.id}
    
    }

    getData(){
        console.log('Get Request to: ' + "http://192.168.56.104:8080/api/teacher/"+this.state.id)
        axios.get('http://192.168.56.104:8080/api/teacher/'+this.state.id)
        .then(response => {
            this.setState(state => ({teacherData: response.data}));
        }, error => {this.setState(state => ({teacherData: [],})); console.log(error);
    });

    }

    componentDidMount() {
        this.getData(); 
    }

    deleteTeacher(){
        this.result =window.confirm(
            "Вы точно хотите удалить группу?"
        )    
        if (this.result){
            console.log('Delete Request to: ' + "http://192.168.56.104:8080/api/teacher/delete/"+this.state.id)
            axios.delete('http://192.168.56.104:8080/api/teacher/delete/'+this.state.id).then(
                response => {
                    window.location.href = '/e-journal/teachers'
                }
            );
        }
    }

    editTeacher(){
        console.log('Delete Request to: ' + "http://192.168.56.104:8080/api/teacher/delete/"+this.state.id)
        window.location.href = "/e-journal/teacher/edit/" + this.state.id;
    }

    render(){
        if (this.state.teacherData != null)
        return (

            <div className="uk-section uk-section-muted">
                <div  className="uk-margin uk-card uk-card-default uk-card-body">
                    <h1>{this.state.teacherData.surname + ' ' + this.state.teacherData.name + ' ' + this.state.teacherData.patronymic}</h1>
                </div>
               
                <div  className="uk-margin uk-card uk-card-default uk-card-body">
                    <button className="uk-button uk-button-default uk-margin-medium-right" onClick={() => this.deleteTeacher()}>удалить</button>
                    <button className="uk-button uk-button-default" onClick={() => this.editTeacher()}>Редактировать</button>
                </div>
            </div>
        )

    }


}


export default withRouter(TeacherDetail)