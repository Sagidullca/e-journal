import React from "react";
import axios from "axios"

import withRouter from '../withRouter'



class GroupDetail extends React.Component {

    constructor(props){
        super(props);
        this.state ={id : props.params.id}
    
    }

    getData(){
        console.log('Get Request to: ' + "http://192.168.56.104:8080/api/group/"+this.state.id)
        axios.get('http://192.168.56.104:8080/api/group/'+this.state.id)
        .then(response => {
            this.setState(state => ({groupData: response.data}));
        }, error => {this.setState(state => ({groupData: [],})); console.log(error);
    });

    }

    componentDidMount() {
        this.getData(); 
    }

    deleteGroup(){
        this.result =window.confirm(
            "Вы точно хотите удалить группу?"
        )    
        if (this.result){
            console.log('Delete Request to: ' + "http://192.168.56.104:8080/api/group/"+this.state.id)
            axios.delete('http://192.168.56.104:8080/api/group/'+this.state.id).then(
                response => {
                    window.location.href = '/'
                }
            );
        }
    }

    renderStudents(){

        if (this.state.groupData.students.length < 1)
            return(
                <tr><th>no students</th></tr>
                
            )
        else
            return this.state.groupData.students.map((dataRow) =>{
                return (
                    <tr>
                        <th>{dataRow.surname}</th>
                        <th>{dataRow.name}</th>
                        <th>{dataRow.patronymic}</th>
                    </tr>
                )
            })
    }

    renderDiscipline(){

        if (this.state.groupData.disciplines.length < 1)
            return(
                <tr><th>no discipline</th></tr>
                
            )
        else
            return this.state.groupData.disciplines.map((dataRow) =>{
                return (
                    <tr>
                        <th>{dataRow.discipline.name}</th>
                    </tr>
                )
            })
    }

    editGroup(){
        console.log('Delete Request to: ' + "http://192.168.56.104:8080/api/group/edit/"+this.state.id)
        window.location.href = "/group/edit/" + this.state.id;
    }

    markGroup(){
        window.location.href = "/e-journal/group/mark/"+this.state.id
    }



    render(){
        if (this.state.groupData != null)
        return (

            <div className="uk-section uk-section-muted">
                <div  className="uk-margin uk-card uk-card-default uk-card-body">
                    <h1>Номер группы: {this.state.groupData.group.number}</h1>
                    <p><strong>Специальность: {this.state.groupData.group.specialization}</strong></p>
                </div>
                <div  className="uk-margin uk-card uk-card-default uk-card-body">
                    <ul class="uk-tab-bottom" data-uk-tab="{connect:'#group_datas'}">
                        <li><a href="students">Студенты</a></li>
                        <li><a href="#">Дисциплины</a></li>
                    </ul>
                    <ul id="group_data" class="uk-switcher uk-margin">
                        <li>
                            <div id="students" >
                                <table  className="uk-table uk-table-striped">
                                    <thead>
                                        <tr>
                                            <th>Фамилия</th>
                                            <th>Имя</th>
                                            <th>Отчество</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {this.renderStudents()}
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li>
                        <div id="discipline" >
                                <table  className="uk-table uk-table-striped">
                                    <thead>
                                        <tr>
                                            <th>Название дисциплины</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {this.renderDiscipline()}
                                    </tbody>
                                </table>
                            </div>
                        </li>
                    </ul>
                </div>

                <div  className="uk-margin uk-card uk-card-default uk-card-body">
                    <button className="uk-button uk-button-default" onClick={() => this.deleteGroup()}>Удалить</button>
                    <button className="uk-button uk-button-default" onClick={() => this.editGroup()}>Редактировать</button>
                    <button className="uk-button uk-button-default" onClick={() => this.markGroup()}>Журнал оценок</button>
                </div>
            </div>
        )

    }


}


export default withRouter(GroupDetail)