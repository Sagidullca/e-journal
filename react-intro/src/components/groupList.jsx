import React from "react";
import axios from "axios"
import {
    Link,
  } from 'react-router-dom';


class GroupList extends React.Component {

    getData(){
        console.log('Get Request to: ' + "http://192.168.56.104:8080/api/groups")
        axios.get("http://192.168.56.104:8080/api/groups")
        .then(response => {
            this.setState(state => ({groupsData: response.data}));
        }, error => {this.setState(state => ({groupsData: [],})); console.log(error);
    });

    }

    constructor(props){
        super(props);
        this.state = {groupsData: []};
    }

    componentDidMount() {
        this.getData(); 
    }


    renderData() {
        if (this.state.groupsData.length > 0 ){
            return this.state.groupsData.map((dataRow) =>{
                return (
                    <Link key={dataRow.id} to={`/e-journal/group/${dataRow.id}`}>
                        <li>{dataRow.number}</li>
                    </Link> 
                )
            })
        }
        else {
            return(
                <div className="uk-alert-danger">
                    <a className="uk-alert-close"></a>
                    <p>NO GROUPS</p>
                </div>  
            )
        }
    }

    render() {
        return(
        <div className="uk-section uk-section-muted">
            <div  className="uk-margin uk-card uk-card-default uk-card-body">
                <title>Учебный группы</title>
                <h1>Список групп</h1>
                <Link key="create" to="/group/create">
                    <a>Добавить новую группу</a>
                </Link>
                <br/>
               
                        <ul>
                            {this.renderData()}
                        </ul>
                
            </div>
        </div>
        );
    }
}

export default GroupList