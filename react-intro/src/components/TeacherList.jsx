import React from "react";
import axios from "axios"
import {
    Link,
  } from 'react-router-dom';


class TeacherList extends React.Component {

    getData(){
        console.log('Get Request to: ' + "http://192.168.56.104:8080/api/teachers")
        axios.get("http://192.168.56.104:8080/api/teachers")
        .then(response => {
            this.setState(state => ({teacherData: response.data}));
        }, error => {this.setState(state => ({teacherData: [],})); console.log(error);
    });

    }

    constructor(props){
        super(props);
        this.state = {teacherData: []};
    }

    componentDidMount() {
        this.getData(); 
    }


    renderData() {
        if (this.state.teacherData.length > 0 ){
            return this.state.teacherData.map((dataRow) =>{
                return (
                    <Link key={dataRow.id} to={`/e-journal/teacher/${dataRow.id}`}>
                        <li>{dataRow.surname + ' ' + dataRow.name }</li>
                    </Link> 
                )
            })
        }
        else {
            return(
                <div className="uk-alert-danger">
                    <a className="uk-alert-close"></a>
                    <p>NO Teachers</p>
                </div>  
            )
        }
    }

    render() {
        return(
        <div className="uk-section uk-section-muted">
            <div  className="uk-margin uk-card uk-card-default uk-card-body">
                <h1>Список преподавателей</h1>
                <Link key="create" to="/teacher/create">
                    <a>Добавить нового преподавателя</a>
                </Link>
                <br/>
               
                        <ul>
                            {this.renderData()}
                        </ul>
                
            </div>
        </div>
        );
    }
}

export default TeacherList