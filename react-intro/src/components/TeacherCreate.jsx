import React  from "react";
import axios from "axios"
import withRouter from '../withRouter'



class TeacherCreate extends React.Component {

    constructor(props){
        super(props);  
    }

   updateData = (event) => {
        console.log('POST Request to: ' + "http://192.168.56.104:8080/api/teacher/")
        event.preventDefault(); 
        // формируем данные для отправки на сервер
        let data = {
          surname: (this.state.surname), 
          name: (this.state.name), 
          patronymic: (this.state.patronymic)
          
        };
        // HTTP-клиент axios автоматически преобразует объект data в json-строку
        axios.post("http://192.168.56.104:8080/api/teacher/create", data, {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json;charset=UTF-8",
          },
        })
        .then(response => {
          console.log('Response: ' + response.status);
          window.location.href = '/e-journal/teachers'
        }, error => {
            console.log(error);
            alert(error);
        });
      }

    render(){
        return (
            <form onSubmit={this.updateData} className="uk-form-stacked">
                <div className="uk-section uk-section-muted">
                    <div  className="uk-margin uk-card uk-card-default uk-card-body">
                    <label className="uk-form-label">Фамилия:</label>
                    <input className= "uk-input"  type="text" minLength="4" maxLength="20" onChange={(e) => {this.setState({surname: e.target.value})}}></input>
                    <label className="uk-form-label">Имя:</label>
                    <input className= "uk-input" type="text" maxLength="20" onChange={(e) => {this.setState({name: e.target.value})}}/>
                    <label className="uk-form-label">Отчество:</label>
                    <input className= "uk-input" type="text" maxLength="20" onChange={(e) => {this.setState({patronymic: e.target.value})}}/>

                    <input type="submit" value="Сохранить" className="uk-button uk-button-primary uk-margin-small"/>
                    </div>
                </div>
            </form>
        )

    }


}


export default withRouter(TeacherCreate)