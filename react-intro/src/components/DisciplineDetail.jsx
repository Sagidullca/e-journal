import React from "react";
import axios from "axios"

import withRouter from '../withRouter'



class DisciplineDetail extends React.Component {

    constructor(props){
        super(props);
        this.state ={id : props.params.id}
    
    }

    getData(){
        console.log('Get Request to: ' + "http://192.168.56.104:8080/api/discipline/"+this.state.id)
        axios.get('http://192.168.56.104:8080/api/discipline/'+this.state.id)
        .then(response => {
            this.setState(state => ({disciplineData: response.data}));
        }, error => {this.setState(state => ({teacherData: [],})); console.log(error);
    });

    }

    componentDidMount() {
        this.getData(); 
    }

    deleteDiscipline(){
        this.result =window.confirm(
            "Вы точно хотите удалить ?"
        )    
        if (this.result){
            console.log('Delete Request to: ' + "http://192.168.56.104:8080/api/discipline/delete/"+this.state.id)
            axios.delete('http://192.168.56.104:8080/api/discipline/delete/'+this.state.id).then(
                response => {
                    window.location.href = '/e-journal/disciplines'
                }
            );
        }
    }

    editDiscipline(){
        console.log('Delete Request to: ' + "http://192.168.56.104:8080/api/discipline/edit/"+this.state.id)
        window.location.href = "/discipline/edit/" + this.state.id;
    }

    render(){
        if (this.state.disciplineData != null)
        return (

            <div className="uk-section uk-section-muted">
                <div  className="uk-margin uk-card uk-card-default uk-card-body">
                    <h1>Название: {this.state.disciplineData.name}</h1>
                    <h2>Сокращение:  {this.state.disciplineData.short_name}</h2>
                    <h2>Преподаватель: {this.state.disciplineData.teacher.surname + ' ' +
                    this.state.disciplineData.teacher.name + ' '+
                    this.state.disciplineData.teacher.patronymic}</h2>
                    <h2>курс: {this.state.disciplineData.course}</h2>
                    <h2>количество часов: {this.state.disciplineData.hours}</h2>
                    <h2>тип экзамена:  {this.state.disciplineData.type_exam == "Z" ? "Зачет": this.state.disciplineData.type_exam == "E"? "Экзамен": this.state.disciplineData.type_exam == "O" ? "Зачет с оценкой" : "Неизвестно"   } </h2>
                </div>
               
                <div  className="uk-margin uk-card uk-card-default uk-card-body">
                    <button className="uk-button uk-button-default uk-margin-medium-right" onClick={() => this.deleteDiscipline()}>удалить</button>
                    <button className="uk-button uk-button-default" onClick={() => this.editDiscipline()}>Редактировать</button>
                </div>
            </div>
        )

    }


}


export default withRouter(DisciplineDetail)