import React  from "react";
import axios from "axios"
import withRouter from '../withRouter'
import { ThemeConsumer } from "styled-components";



class GroupEdit extends React.Component {

    constructor(props){
        super(props);
        this.state ={id : props.params.id, total_disciplines: [], deleted_disciplines: [],  deleted_students: []}

        
    }

    updateData = (event) => {
        console.log('POST Request to: ' + "http://192.168.56.104:8080/api/group/edit/")
        // получаем Id населённого пункта из словаря и меняем состояние через встроенный метод класса React.Component setState
        event.preventDefault();   // необходимо, чтобы отключить стандартное поведение формы в браузере (AJAX)
        // формируем данные для отправки на сервер
        let data = {
          group: {number: this.state.number, specialization: this.state.specialization}, 
          students: this.state.students.concat(this.state.deleted_students),
          disciplines: this.state.disciplines.concat( this.state.deleted_disciplines)
          
        };
        // HTTP-клиент axios автоматически преобразует объект data в json-строку
        axios.put("http://192.168.56.104:8080/api/group-edit/"+this.state.id, data, {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json;charset=UTF-8",
          },
        })
        .then(response => {
          console.log('Response: ' + response.status);
          window.location.href = '/e-journal/'
        }, error => {
            console.log(error);
            alert(error);
        });
      }

    
    
    


    getData(){
        console.log('Get Request to: ' + "http://192.168.56.104:8080/api/group/"+this.state.id)
        axios.get('http://192.168.56.104:8080/api/group/'+this.state.id)
        .then(response => {
            this.setState(state => ({number: response.data.group.number}));
            this.setState(state => ({specialization: response.data.group.specialization}));
            this.setState(state => ({students: response.data.students}));
            this.setState(state => ({disciplines: response.data.disciplines}));
            this.setState(state => ({gettingData: true}))
        }, error => {this.setState(state => ({groupData: [],})); console.log(error);
    });
        axios.get('http://192.168.56.104:8080/api/disciplines')
        .then(response => {
            this.setState(state => ({total_disciplines: response.data}))
        }, error => {this.setState(state => ({total_disciplines: []})); console.log(error)});
      

    }


      
    componentDidMount() {
        this.getData(); 
    }

    
    renderStudents(){

        
            if (this.state.students.length > 0)
            
            return this.state.students.map((dataRow, index) =>{
                return (
                    <tr>
                        <th><input className= "uk-input"value={dataRow.surname} name="surname"  onChange={(event) => this.changeNoteStudent(index, event)}/></th>
                        <th><input className= "uk-input" value={dataRow.name} name="name" onChange={(event) => this.changeNoteStudent(index, event)}/></th>
                        <th><input className= "uk-input" value={dataRow.patronymic} name="patronymic"  onChange={(event) => this.changeNoteStudent(index, event)}/></th>
                        <th><button type="button" className="uk-button" onClick={e => this.handleRemoveClickStudent(index)}>Отчислить</button></th>
                    </tr>
                )
            })
    }

    

    renderDiscipline(){
        if (this.state.disciplines.length > 0)
            return this.state.disciplines.map((dataRow, index) =>{
                return (
                    <tr key={index}>
                        <th>
                            <select className="uk-select" value={dataRow.discipline_id} onChange={(event) => this.changeNoteDiscipline(index, event)}>
                                {
                                    this.state.total_disciplines.map((data) => <option value={data.id}> {data.name}</option>)
                                }    
                            </select></th>
                            <th><button type="button" className="uk-button" onClick={e => this.handleRemoveClickDiscipline(index)}>Удалить</button></th>
                    </tr>
                )
            })
    }

    changeNoteDiscipline(index, event) {
        const copy = [...this.state.disciplines];
        copy[index].discipline_id = event.target.value;
        this.setState(state => ({disciplines: copy}));
    }

    changeNoteStudent(index, event) {
        const copy = [...this.state.students];
        copy[index][event.target.name] = event.target.value;
        this.setState(state => ({students: copy}));
    }

    handleRemoveClickDiscipline(index) {
        const list = [...this.state.disciplines];
        if (this.state.disciplines[index].extra_id !== -1)
            this.setState(state => ({deleted_disciplines: [...state.deleted_disciplines, {id: this.state.disciplines[index].id, type: -1}]}))
        list.splice(index, 1);
        this.setState(state => ({disciplines: list}));
      };

      handleRemoveClickStudent(index) {
        const list = [...this.state.students];
        if (this.state.students[index].id !== -1)
            this.setState(state => ({deleted_students: [...state.deleted_students, {id: this.state.students[index].id, type: -1}]}))
        list.splice(index, 1);
        this.setState(state => ({students: list}));
      };
       

    addStudent(){
        var data = {id: -1, surname: '', name: "", patronymic: "", type: 1 }// this.state.groupData.students
        this.setState(state => ({ students: [...state.students, data]
        }));

    }

    addDiscipline(){
        var data = {discipline_id: -1, id: -1, type: 1}// this.state.groupData.students
        this.setState(state => ({ disciplines: [...state.disciplines, data]
        }));

    }

    render(){
        if (this.state.gettingData != null)
        return (

            <form onSubmit={this.updateData} className="uk-form-stacked">
                <div className="uk-section uk-section-muted">
                    <div  className="uk-margin uk-card uk-card-default uk-card-body">
                        <h1>Номер группы:</h1><input className= "uk-input" id="groupNumber" type="number" minLength="4" maxLength="8" value={this.state.number} onChange={(e) => { this.setState(state => ({number: e.target.value}));}}></input>
                        <p><strong>Специальность:</strong> <input className= "uk-input" type="text" maxLength="300" value= {this.state.specialization} onChange={(e) => { this.setState(state => ({
            specialization: e.target.value
        }));}}/></p>
                        <ul class="uk-tab-bottom" data-uk-tab="{connect:'#group_datas'}">
                        <li><a href="students">Студенты</a></li>
                        <li><a href="#">Дисциплины</a></li>
                    </ul>
                    <ul id="group_data" class="uk-switcher uk-margin">
                        <li>
                            <div id="students" >
                                <button type="button" className="uk-button" onClick={this.addStudent.bind(this)}>Добавить студента</button>
                                <table  className="uk-table uk-table-striped">
                                    <thead>
                                        <tr>
                                            <th>Фамилия</th>
                                            <th>Имя</th>
                                            <th>Отчество</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {this.renderStudents()}
                                    </tbody>
                                </table>
                            </div>
                        </li>
                        <li>
                        <div id="discipline" >
                                <button type="button" className="uk-button" onClick={this.addDiscipline.bind(this)}>Добавить дисциплину</button>
                                <table  className="uk-table uk-table-striped">
                                    <thead>
                                        <tr>
                                            <th>Название дисциплины</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {this.renderDiscipline()}
                                    
                                    </tbody>
                                </table>
                            </div>
                        </li>
                    </ul>
                       
                        <input type="submit" value="Сохранить" className="uk-button uk-button-primary"/>
                    </div>
                </div>
                
            </form>
        )

    }


}


export default withRouter(GroupEdit)