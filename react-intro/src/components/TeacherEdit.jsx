import React from "react";
import axios from "axios"

import withRouter from '../withRouter'



class TeacherEdit extends React.Component {

    constructor(props){
        super(props);
        this.state ={id : props.params.id}
    
    }

    getData(){
        console.log('Get Request to: ' + "http://192.168.56.104:8080/api/teacher/"+this.state.id)
        axios.get('http://192.168.56.104:8080/api/teacher/'+this.state.id)
        .then(response => {
            this.setState(state => ({surname: response.data.surname}));
            this.setState(state => ({name:  response.data.name}));
            this.setState(state => ({patronymic:  response.data.patronymic}));
        }, error => {this.setState(state => ({})); console.log(error);
    });

    }

    componentDidMount() {
        this.getData(); 
    }

    updateData = (event) => {
        console.log('POST Request to: ' + "http://192.168.56.104:8080/api/teacher/edit")
        event.preventDefault(); 
        // формируем данные для отправки на сервер
        let data = {
          id: (this.state.id),
          surname: (this.state.surname), 
          name: (this.state.name), 
          patronymic: (this.state.patronymic)
          
        };
        // HTTP-клиент axios автоматически преобразует объект data в json-строку
        axios.patch("http://192.168.56.104:8080/api/teacher/edit/"+this.state.id, data, {
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json;charset=UTF-8",
          },
        })
        .then(response => {
          console.log('Response: ' + response.status);
          window.location.href = '/teachers'
        }, error => {
            console.log(error);
            alert(error);
        });
      }

    render(){
        return (

            <form onSubmit={this.updateData} className="uk-form-stacked">
            <div className="uk-section uk-section-muted">
                <div  className="uk-margin uk-card uk-card-default uk-card-body">
                <label className="uk-form-label">Фамилия:</label>
                <input className= "uk-input"  type="text" minLength="4" value={this.state.surname} maxLength="20" onChange={(e) => {this.setState({surname: e.target.value})}}></input>
                <label className="uk-form-label">Имя:</label>
                <input className= "uk-input" type="text" maxLength="20" value={this.state.name}  onChange={(e) => {this.setState({name: e.target.value})}}/>
                <label className="uk-form-label">Отчество:</label>
                <input className= "uk-input" type="text" maxLength="20" value={this.state.patronymic} onChange={(e) => {this.setState({patronymic: e.target.value})}}/>

                <input type="submit" value="Сохранить" className="uk-button uk-button-primary uk-margin-small"/>
                </div>
            </div>
            </form>
        )

    }


}


export default withRouter(TeacherEdit)